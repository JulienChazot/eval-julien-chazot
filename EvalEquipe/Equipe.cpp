#include "Equipe.h"
#include <iostream>

using namespace std;

/// <summary>
/// Initialisation de la valeur par d�faut de l'attribut statique tailleEquipe
/// </summary>
int Equipe::tailleEquipe = 5;

/// <summary>
/// Contructeur
/// </summary>
/// <param name="nom">Le nom � affecter � l'�quipe</param>
/// <param name="numero">Le num�ro � affecter � l'�quipe</param>
Equipe::Equipe(std::string& nom, const int& numero)  :  nomEquipe(nom),
														numeroEquipe(numero)
{
#ifdef _DEBUG
	cout << "--> Equipe("<< nom << ", " << numero << ") de " << this << endl;
#endif 
}

/// <summary>
/// Destructeur
/// </summary>
Equipe::~Equipe()
{
#ifdef _DEBUG
	cout << "--> ~Equipe() de " << this << endl;
#endif 
}

/// <summary>
/// Permet de savoir si l'�quipe est compl�te
/// </summary>
/// <returns>true si l'�quipe est compl�te</returns>
bool Equipe::EquipeEstComplete()
{
	cout << "--> TODO: D�velopper Equipe::EquipeEstComplete()" << endl;
	return false;
}

/// <summary>
/// Ajoute un membre � l'�quipe, si elle n'est pas compl�te.
/// </summary>
/// <param name="membre">Le dossard du membre � ajouter</param>
/// <returns>true s'il reste de la place</returns>
bool Equipe::AjouterMembre(int dossard)
{
	cout << "--> TODO: D�velopper Equipe::AjouterMembre()" << endl;
	return false;
}

