#include "Concurrent.h"
#include <iostream>

using namespace std;

Concurrent::Concurrent(string& nom, const int& dossard): nom(nom), dossard(dossard)
{
#ifdef _DEBUG
	cout << "--> Concurrent(" << nom << ") de " << this << endl;
#endif
}

Concurrent::Concurrent(const Concurrent& src):nom(src.nom), dossard(src.dossard), score(src.score)
{
#ifdef _DEBUG
	cout << "--> Concurrent(Concurrent&) de " << this << " � partir de " << &src << endl;
#endif
}

Concurrent::~Concurrent()
{
#ifdef _DEBUG
	cout << "--> ~Concurrent() de " << this << endl;
#endif
}

std::ostream& operator<<(std::ostream& os, const Concurrent& src)
{
	os << "(" << src.GetDossard() << ") " << src.GetNom() << ": " << src.GetScore();
	return os;
}
